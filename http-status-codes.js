// HTTP Status Codes
// https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
const statuses = [
  { code: 100, message: 'CONTINUE' },
  { code: 101, message: 'SWITCHING_PROTOCOLS' },
  { code: 200, message: 'OK' },
  { code: 201, message: 'CREATED' },
  { code: 202, message: 'ACCEPTED' },
  { code: 203, message: 'NON_AUTHORITATIVE_INFORMATION' },
  { code: 204, message: 'NO_CONTENT' },
  { code: 205, message: 'RESET_CONTENT' },
  { code: 206, message: 'PARTIAL_CONTENT' },
  { code: 300, message: 'MULTIPLE_CHOICES' },
  { code: 301, message: 'MOVED_PERMANENTLY' },
  { code: 302, message: 'FOUND' },
  { code: 303, message: 'SEE_OTHER' },
  { code: 304, message: 'NOT_MODIFIED' },
  { code: 305, message: 'USE_PROXY' },
  { code: 306, message: '(UNUSED)' },
  { code: 307, message: 'TEMPORARY_REDIRECT' },
  { code: 400, message: 'BAD_REQUEST' },
  { code: 401, message: 'UNAUTHORIZED' },
  { code: 402, message: 'PAYMENT_REQUIRED' },
  { code: 403, message: 'FORBIDDEN' },
  { code: 404, message: 'NOT_FOUND' },
  { code: 405, message: 'METHOD_NOT_ALLOWED' },
  { code: 406, message: 'NOT_ACCEPTABLE' },
  { code: 407, message: 'PROXY_AUTHENTICATION_REQUIRED' },
  { code: 408, message: 'REQUEST_TIMEOUT' },
  { code: 409, message: 'CONFLICT' },
  { code: 410, message: 'GONE' },
  { code: 411, message: 'LENGTH_REQUIRED' },
  { code: 412, message: 'PRECONDITION_FAILED' },
  { code: 413, message: 'REQUEST_ENTITY_TOO_LARGE' },
  { code: 414, message: 'REQUEST_URI_TOO_LONG' },
  { code: 415, message: 'UNSUPPORTED_MEDIA_TYPE' },
  { code: 416, message: 'REQUEST_RANGE_NOT_SATISFIABLE' },
  { code: 417, message: 'EXPECTATION_FAILED' },
  { code: 500, message: 'INTERNAL_SERVER_ERROR' },
  { code: 501, message: 'NOT_IMPLEMENTED' },
  { code: 502, message: 'BAD_GATEWAY' },
  { code: 503, message: 'SERVICE_UNAVAILABLE' },
  { code: 504, message: 'GATEWAY_TIMEOUT' },
  { code: 505, message: 'HTTP_VERSION_NOT_SUPPORTED' }
]

// Convert: code → message or message → code
module.exports.convert = (codeOrMessage) => {
  statuses.find((status) => {
    if (status.code === codeOrMessage) {
      return status.message
    } else if (status.message === codeOrMessage) {
      return status.code
    } else {
      return null
    }
  })
}
